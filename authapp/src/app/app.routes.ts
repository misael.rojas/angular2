import { RouterModule, Routes } from '@angular/router';

import {HomeComponent} from './components/home/home.component';
import {PricingComponent} from './components/pricing/pricing.component';
import {PrivateComponent} from './components/private/private.component';

import { AuthGuardService } from './services/auth-guard.service';

const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'pricing', component: PricingComponent },
  {
    path: 'private', component: PrivateComponent,
    canActivate: [ AuthGuardService ]
  },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);

import { Component } from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styles: [`
    input.ng-invalid.ng-touched {
        border: 1px solid red;
    }
  `]
})
export class TemplateComponent {

  /*user: Object = {
    name: 'Ai',
    lastname: 'Takahashi',
    email: 'ai.takahashi.misa@gmail.com'
  };*/

  user: Object = {
    name: null,
    lastname: null,
    email: null
  };

  constructor( ) { }

  save( forma: NgForm ) {
    console.log('form submited');
    console.log('form', forma); // full
    console.log('values', forma.value);
  }


}

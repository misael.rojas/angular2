import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'withoutPhoto'
})
export class WithoutPhotoPipe implements PipeTransform {

  transform(value: any[]): string {

    let noiamge = "assets/img/noimage.png"

    if ( !value) {
        return noiamge;
    }

    return ( value.length > 0 )? value[1].url : noiamge ;
  }

}

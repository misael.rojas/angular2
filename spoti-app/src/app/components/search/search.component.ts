import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent implements OnInit {

  searchTerm:string = "";

  constructor( private _spotifyService:SpotifyService ) { }

  ngOnInit() {}

  searchArtist () {
    this._spotifyService.getArtists(this.searchTerm).subscribe();
  }

}

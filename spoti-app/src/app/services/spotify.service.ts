import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SpotifyService {

  artists:any [] = [];

  urlSearch:string = "https://api.spotify.com/v1/search";
  urlArtist:string = "https://api.spotify.com/v1/artists";

  constructor( private http:Http ) { }

  getArtists ( artist:string ) {

    let headers = new Headers();
    headers.append( 'authorization', 'Bearer BQDWNMA0BVok2NlqTI4WTFkOY8uf-dH6AvQ9onp5HoklsNQUnhld_JQsNGziYsTJnxLVYRmZksPDfx0u_2tgPA')

    let query=`?q=${ artist }&type=artist`
    let url= this.urlSearch + query

    return this.http.get( url, { headers } )
      .map( res =>{
          //console.log(res);
          //console.log(res.json());
          this.artists=res.json().artists.items;
          console.log( this.artists );
          return this.artists;
    })

  }

  getArtist ( id:string ) {
    let headers = new Headers();
    headers.append( 'authorization', 'Bearer BQDWNMA0BVok2NlqTI4WTFkOY8uf-dH6AvQ9onp5HoklsNQUnhld_JQsNGziYsTJnxLVYRmZksPDfx0u_2tgPA')

    let query=`/${ id }`
    let url= this.urlArtist + query



    return this.http.get( url, { headers } )
      .map( res =>{
          //console.log(res);
          console.log(res.json());
          return res.json();
    })
  }

  getTop ( id:string ) {
    let headers = new Headers();
    headers.append( 'authorization', 'Bearer BQDWNMA0BVok2NlqTI4WTFkOY8uf-dH6AvQ9onp5HoklsNQUnhld_JQsNGziYsTJnxLVYRmZksPDfx0u_2tgPA')

    let query=`/${ id }/top-tracks?country=MX`
    let url= this.urlArtist + query

    return this.http.get( url, { headers } )
      .map( res =>{
          //console.log(res);
          console.log(res.json().tracks);
          return res.json().tracks;
    })
  }

}

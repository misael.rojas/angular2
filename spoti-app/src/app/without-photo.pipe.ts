import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'withoutPhoto'
})
export class WithoutPhotoPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return null;
  }

}

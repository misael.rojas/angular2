import { Component, OnInit, OnChanges, DoCheck, AfterContentInit,
AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
  <app-ng-style></app-ng-style>
  <app-css></app-css>
  <app-classes></app-classes>

  <hr>

  <br><br>
  <h3>Custom Directives</h3>
  <p [appResaltado]="'orange'">
    いい子にしていたら、飴をあげよう
  </p>
  <br><br>
  <p [appResaltado]="'skyblue'">
    すみませんが、もう少しまってくれませんか
  </p>

  <app-ng-switch></app-ng-switch>
  `,
  styles: []
})
export class HomeComponent implements OnInit, OnChanges, DoCheck, AfterContentInit,
AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {

  constructor() { }

  ngOnInit() {
    console.log('ngOnInit')
  }

  ngOnChanges(){
    console.log('ngOnChanges')
  }

  ngDoCheck(){
    console.log('ngDoCheck')
  }

  ngAfterContentInit(){
    console.log('ngAfterContentInit')
  }

  ngAfterContentChecked(){
    console.log('ngAfterContentChecked')
  }

  ngAfterViewInit(){
    console.log('ngAfterViewInit')
  }

  ngAfterViewChecked(){
    console.log('ngAfterViewChecked')
  }

  ngOnDestroy(){
    console.log('ngOnDestroy')
  }


}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-style',
  template: `
    <p  [style.fontSize.px]="sizeFont">
      ng-style Works! <i class="fa fa-star"></i>
    </p>

    <button class="btn btn-primary" (click)="sizeFont = sizeFont + 5"> <i class="fa fa-plus"></i></button>
    <button class="btn btn-danger" (click)="sizeFont = sizeFont - 5"> <i class="fa fa-minus"></i></button>


  `,
  styles: []
})
export class NgStyleComponent implements OnInit {

  sizeFont:number = 15

  constructor() { }

  ngOnInit() {
  }

}

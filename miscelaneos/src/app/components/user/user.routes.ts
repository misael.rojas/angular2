import { Routes } from '@angular/router';

import { NewUserComponent } from './new-user.component';
import { UserDetailsComponent } from './user-details.component';
import { UserEditComponent } from './user-edit.component';

export const USER_ROUTES: Routes = [
  { path: 'new', component: NewUserComponent },
  { path: 'edit', component: UserEditComponent },
  { path: 'details', component: UserDetailsComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'new' }
];

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-css',
  template: `
    <hr>  
    <p>
      CSS for this component only
    </p>
  `,
  styles: [`
  p {
    color:red
  }
  `]
})
export class CssComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styles: []
})
export class ClassesComponent implements OnInit {

  alertType="alert-success"

  loading:boolean = false;

  constructor() { }

  ngOnInit() {
  }

  load(){
    this.loading = true;

    setTimeout( ()=> this.loading = false, 3000 )
  }


}

import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {


  constructor( private el:ElementRef ) {
    console.log('directive called')
    //el.nativeElement.style.backgroundColor='red'
    //el.nativeElement.style.color='white'
    el.nativeElement.style.padding='5px 10px'
  }

  @Input('appResaltado') newColor:string

  @HostListener('mouseenter') mouseEntro(){
    console.log('change color to ' + this.newColor)

    this.resaltar ( this.newColor || 'purple')
  }

  @HostListener('mouseleave') mouseSalio(){
    this.resaltar ( null )
  }

  private resaltar ( color: string){

    this.el.nativeElement.style.backgroundColor=color
  }

}
